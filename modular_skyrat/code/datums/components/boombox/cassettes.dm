//Cassette subtypes
/obj/item/device/cassette/os13
	name = "cassette volume 1"
	desc = "This tune reminds you of a distant station."
	icon_state = "vol_1"
	all_tunes = list(
					"Space Song" = 'modular_skyrat/sound/music/space_song.ogg',
					)

/obj/item/device/cassette/manwhosoldtheworld
	name = "cassette volume 2 - The Man Who Sold The World"
	desc = "You're face, to face."
	icon_state = "vol_2"
	all_tunes = list(
					"Midge Ure - Man Who Sold The World" = 'modular_skyrat/sound/music/midgeure_manwhosoldtheworld.ogg',
					"David Bowie - Man Who Sold The World" = 'modular_skyrat/sound/music/davidbowie_manwhosoldtheworld.ogg',
					)

/obj/item/device/cassette/everywhereattheendoftime
	name = "cassette volume 3 - Everywhere At The End Of Time"
	desc = "Uhhhhhhhh..."
	icon_state = "vol_3"
	all_tunes = list(
					"The Caretaker - It's Just a Burning Memory" = 'modular_skyrat/sound/music/itsjustaburningmemory.ogg',
					"The Caretaker - Back There Benjamin" = 'modular_skyrat/sound/music/backtherebenjamin.ogg',
					"The Caretaker - Place in the world fades away" = 'modular_skyrat/sound/music/placeintheworldfadesaway.ogg',
					)

/obj/item/device/cassette/doom
	name = "cassette volume 4 - DOOM"
	desc = "Mhm... That's one doomed death commando."
	icon_state = "vol_4"
	all_tunes = list(
					"Bobby Prince - E1M1" = 'modular_skyrat/sound/music/e1m1.mid',
					"Mick Gordon - BFG Divison" = 'modular_skyrat/sound/music/bfgdivision.ogg',
					"Mick Gordon - Mastermind" = 'modular_skyrat/sound/music/mastermind.ogg',
					"Mick Gordon - The Only Thing They Fear Is You" = 'modular_skyrat/sound/music/theonlythingtheyfearisyou.ogg',
					)

/obj/item/device/cassette/irreversible
	name = "cassette volume 5 - Irreversible"
	desc = "Rapist music."
	icon_state = "vol_5"
	all_tunes = list(
					"Thomas Bangalter - Stress" = 'modular_skyrat/sound/music/stress.ogg',
					"Thomas Bangalter - Rectum" = 'modular_skyrat/sound/music/rectum.ogg',
					"Thomas Bangalter - Spinal Scratch" = 'modular_skyrat/sound/music/spinalscratch.ogg',
					)

/obj/item/device/cassette/dreamer
	name = "cassette volume #$%&"
	desc = "Sweet dreams."
	icon_state = "vol_?"
	all_tunes = list(
					"Judge Bitch - Hot Plates" = 'modular_skyrat/sound/music/hot_plates.ogg',
					"Mega Drive - Converter" = 'modular_skyrat/sound/music/converter.ogg',
					"CABLE - Spires" = 'modular_skyrat/sound/music/converter.ogg',
					)
