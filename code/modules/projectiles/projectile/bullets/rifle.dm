// 5.56mm (M-90gl Carbine)

/obj/item/projectile/bullet/a556
	name = "5.56mm bullet"
	damage = 35
	wound_bonus = -10

// 7.62 (Nagant Rifle)

/obj/item/projectile/bullet/a762
	name = "7.62 bullet"
	damage = 65    //Skyrat edit: Only obtainable branded gun that fires this caliber is the Mosin I believe. Assume it's 7.62x54mm Rimmed.

/obj/item/projectile/bullet/a762_enchanted
	name = "enchanted 7.62 bullet"
	damage = 5
	pain = 30
	stamina = 50
	wound_bonus = CANT_WOUND
